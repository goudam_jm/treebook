require 'test_helper'

class ProfilesControllerTest < ActionController::TestCase
  test "should get show" do
    get :show, id: users(:goudam).profile_name
    assert_response :success
    assert_template "profiles/show"
  end

  test "should render a 404 for profile not found" do
  	get :show, id: "doesnt exist"
  	assert_response :not_found

  end

  test "Variables are assigned correctly on successful profile view" do
  	get :show, id: users(:goudam).profile_name
  	assert assigns(:user)
  	assert_not_empty assigns(:statuses)
  end

  test "only shows statuses for that user" do
  	get :show, id: users(:goudam).profile_name
  	assigns(:statuses).each do |status|
  		assert_equal users(:goudam), status.user
  	end
  end
  
end
