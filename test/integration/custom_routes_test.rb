require 'test_helper'

class CustomRoutesTest < ActionDispatch::IntegrationTest
	test " that /login works properly" do
		get "/login"

		assert_response :success
	end

	test " that /logout works properly" do
		get "/logout"

		assert_response :redirect
		assert_redirected_to '/'
	end

	test " that /register works properly" do
		get "/register"

		assert_response :success
	end

	test "that a profile page works" do
		get "/goudam_jm"
		assert_response :success

	end
end
