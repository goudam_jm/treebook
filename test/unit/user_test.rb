require 'test_helper'

class UserTest < ActiveSupport::TestCase
	test "a user should enter first name" do
		user = User.new
		assert !user.save
		assert !user.errors[:first_name].empty?
	end

		test "a user should enter last name" do
		user = User.new
		assert !user.save
		assert !user.errors[:last_name].empty?
	end

		test "a user should enter profile name" do
		user = User.new
		assert !user.save
		assert !user.errors[:profile_name].empty?
	end

		test "a user should have unique profile name" do
		user = User.new
		user.profile_name = users(:goudam).profile_name

		assert !user.save
		assert !user.errors[:profile_name].empty?
	end

		test "a user should have a profile name without spaces" do
		user = User.new(first_name: 'Goudam', last_name: 'Muralitharan', email: 'goudam_jm2@yahoo.com')
		user.password = user.password_confirmation = "check1234"
		
		user.profile_name = "Goudam Space"

		assert !user.save
		assert !user.errors[:profile_name].empty?
		assert user.errors[:profile_name].include?("Must be formatted correctly")
	end

	test "a user can have a properly formatted profile name" do
		user = User.new(first_name: 'Goudam', last_name: 'Muralitharan', email: 'goudam_jm2@yahoo.com')
		user.password = user.password_confirmation = "check1234"

		user.profile_name = "goudam_jm1"
		assert user.valid?
	end
end